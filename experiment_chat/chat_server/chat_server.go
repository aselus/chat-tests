package chat_server

import (
	"context"
	"encoding/base32"
	"errors"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/aselus/chat-tests/experiment_chat/chat_common/chat_api"
	"math/rand"
	"storj.io/drpc"
	"storj.io/drpc/drpcmetadata"
	"time"
)

type ChatServiceServer struct {
	chat_api.DRPCChatUnimplementedServer
}

func (chatServiceServer *ChatServiceServer) DRPCDescription() drpc.Description {
	return chat_api.DRPCChatDescription{}
}

func New() *ChatServiceServer {
	return &ChatServiceServer{}
}

func (chatServiceServer *ChatServiceServer) SendMessage(ctx context.Context, msg *chat_api.ChatMessage) (*chat_api.Empty, error) {
	metadata, _ := drpcmetadata.Get(ctx)
	decodedId, err := base32.StdEncoding.WithPadding(base32.NoPadding).DecodeString(metadata["id"])
	if err != nil {
		return nil, errors.New("invalid id in token, cannot send message")
	}
	msg.MessageId = decodedId

	//todo(aselus): send to cockroach
	//todo(aselus): send to mq

	return &chat_api.Empty{}, nil
	//return nil, drpcerr.WithCode(errors.New("Unimplemented"), drpcerr.Unimplemented)
}

func (chatServiceServer *ChatServiceServer) GetMessages(_ *chat_api.ChatMessageRequest, responseStream chat_api.DRPCChat_GetMessagesStream) error {
	groupings := [][]byte{uuid.NewV4().Bytes(), uuid.NewV4().Bytes(), uuid.NewV4().Bytes(), uuid.NewV4().Bytes()}
	for i := 0; ; i += 1 {
		grouping := groupings[rand.Intn(4)]
		//these should come from a pool
		if err := responseStream.Send(&chat_api.ChatMessage{
			MessageId:    uuid.NewV4().Bytes(),
			Body:         fmt.Sprintf("%d", i),
			Origin:       uuid.NewV4().Bytes(),
			Grouping:     grouping,
			ReceivedTime: time.Now().UnixNano(),
		}); err != nil {
			return err
		}
		time.Sleep(1 * time.Second)
	}
}
