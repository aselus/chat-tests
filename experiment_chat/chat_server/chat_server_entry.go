package chat_server

import (
	"gitlab.com/aselus/chat-tests/experiment_chat/chat_server/security_server"
	"go.bryk.io/pkg/log"
	"go.bryk.io/pkg/net/drpc"
	"go.bryk.io/pkg/net/drpc/middleware/server"
	"go.bryk.io/pkg/net/drpc/ws"
	"net/http"
	"time"
)

func Start() error {
	// TLS config
	//tlsSettings := drpc.ServerTLS{
	//	Cert:             my-cert-pem,
	//	PrivateKey:       my-key-pem,
	//	CustomCAs:        [][]byte{my-ca-cert-pem},
	//	IncludeSystemCAs: true,
	//}

	smw := []server.Middleware{
		server.Logging(log.WithZero(log.ZeroOptions{true, "error", nil}).Sub(log.Fields{"component": "server"}), nil),
		newAuthTokenHandler(),
		server.PanicRecovery(),
	}

	srv, err := drpc.NewServer(
		drpc.WithServiceProvider(New()),
		drpc.WithPort(8998),
		drpc.WithHTTP(),
		drpc.WithMiddleware(smw...),
		// todo(aselus): for when tls is enabled (even if self signed)
		//drpc.WithTLS(tlsSettings),

		// bi-directional streaming support over WebSockets along with DRPC
		drpc.WithWebSocketProxy(
			ws.EnableCompression(),
			ws.CheckOrigin(func(r *http.Request) bool { return true }),
			ws.HandshakeTimeout(2*time.Second),
			ws.SubProtocols([]string{"rfb", "sip"}),
		))
	if err != nil {
		panic(err)
	}

	securityMw := []server.Middleware{
		server.Logging(log.WithZero(log.ZeroOptions{true, "error", nil}).Sub(log.Fields{"component": "server"}), nil),
		server.PanicRecovery(),
	}

	securityServer, err := drpc.NewServer(
		drpc.WithServiceProvider(security_server.New()),
		drpc.WithPort(8995),
		drpc.WithHTTP(),
		drpc.WithMiddleware(securityMw...),
	)
	if err != nil {
		panic(err)
	}

	go func() {
		if err := securityServer.Start(); err != nil {
			panic(err)
		}
	}()

	if err := srv.Start(); err != nil {
		panic(err)
	}
	return nil
}
