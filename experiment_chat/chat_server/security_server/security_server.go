package security_server

import (
	"context"
	"crypto/rsa"
	"crypto/x509"
	_ "embed"
	"encoding/pem"
	"github.com/golang-jwt/jwt"
	"gitlab.com/aselus/chat-tests/experiment_chat/chat_common/security_api"
	"log"
	"storj.io/drpc"
)

//go:embed 25567571_www.chatbin.com.key
var selfSignedPrivateKey []byte

type SecurityServer struct {
	privateKey *rsa.PrivateKey
	security_api.DRPCSecurityUnimplementedServer
}

func (securityServer *SecurityServer) DRPCDescription() drpc.Description {
	return security_api.DRPCSecurityDescription{}
}

func New() *SecurityServer {
	decodedPrivateKey, _ := pem.Decode(selfSignedPrivateKey)
	if decodedPrivateKey.Type != "RSA PRIVATE KEY" {
		log.Fatal("Invalid private key provided, cannot start security server")
	}

	parsedKey, err := x509.ParsePKCS1PrivateKey(decodedPrivateKey.Bytes)
	if err != nil {
		log.Fatal("Invalid private key provided, cannot start security server")
	}

	return &SecurityServer{
		privateKey: parsedKey,
	}
}

func (securityServer *SecurityServer) Authenticate(_ context.Context, req *security_api.AuthenticationRequest) (*security_api.AuthenticationResponse, error) {
	preSignedToken := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.StandardClaims{
		Id: req.UserId,
	})
	signedToken, err := preSignedToken.SignedString(securityServer.privateKey)
	if err != nil {
		return nil, err
	}

	return &security_api.AuthenticationResponse{Token: signedToken}, nil
}
