package chat_server

import (
	"crypto/rsa"
	"crypto/x509"
	_ "embed"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"go.bryk.io/pkg/net/drpc/middleware/server"
	"storj.io/drpc"
	"storj.io/drpc/drpcmetadata"
)

//go:embed selfsigned_certs/25567571_www.chatbin.com.cert
var selfSignedX509cert []byte

type authToken struct {
	cert      *x509.Certificate
	publicKey *rsa.PublicKey

	next drpc.Handler
}

func newAuthTokenHandler() server.Middleware {
	return func(next drpc.Handler) drpc.Handler {
		block, _ := pem.Decode(selfSignedX509cert)
		cert, _ := x509.ParseCertificate(block.Bytes)
		return authToken{
			next:      next,
			cert:      cert,
			publicKey: cert.PublicKey.(*rsa.PublicKey),
		}
	}
}

func (at authToken) HandleRPC(stream drpc.Stream, rpc string) (err error) {
	data, ok := drpcmetadata.Get(stream.Context())
	if !ok {
		return errors.New("authentication: missing credentials")
	}

	tokenStr, ok := data["auth.token"]
	if !ok {
		return errors.New("authentication: missing credentials")
	}

	if tokenStr == "" {
		return errors.New("authentication: invalid credentials")
	}

	token, err := jwt.ParseWithClaims(tokenStr, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return at.publicKey, nil
	})
	if err != nil {
		fmt.Printf("authentication failed: %v\n", err)
		return err
	}
	data["id"] = token.Claims.(*jwt.StandardClaims).Id

	return at.next.HandleRPC(stream, rpc)
}
