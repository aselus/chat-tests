package chat_server_migrate

import (
	"embed"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"golang.org/x/xerrors"

	_ "github.com/golang-migrate/migrate/v4/database/cockroachdb"
)

//go:embed migrations/*.sql
var migrations embed.FS

func Migrate() error {
	fileSystem, err := iofs.New(migrations, "migrations")
	if err != nil {
		return xerrors.Errorf("Migrate unable to init iofs; %w", err)
	}

	migration, err := migrate.NewWithSourceInstance(
		"iofs",
		fileSystem,
		"cockroachdb://root@localhost:26257/defaultdb?sslmode=disable")
	if err != nil {
		return xerrors.Errorf("Migrate unable to create iofs source; %w", err)
	}

	if err := migration.Up(); err != nil {
		return xerrors.Errorf("Migrate failed to migrate up; %w", err)
	}

	return nil
}
