cd $GOPATH/src/gitlab.com/aselus/chat-tests/experiment_chat
find . -name '*.rpc.proto' -exec protoc -I=. -I=$GOPATH/src --gogoslick_out=$GOPATH/src --go-drpc_out=$GOPATH/src --go-drpc_opt=protolib=gitlab.com/aselus/chat-tests/experiment_chat/gogo_drpc_codec {} \;
find . -name '*.pb.go' ! -iname '*drpc*' -exec easyjson -all {} \;