package chatbincmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/aselus/chat-tests/experiment_chat/chat_client"
)

// clientCmd represents an ascii client
var clientCmd = &cobra.Command{
	Use:   "client",
	Short: "A simple ascii client",
	Long:  `No details thus far above and beyond this being a client for the server.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := chat_client.Start(); err != nil {
			fmt.Println("unable to start server, fatal error:", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(clientCmd)
}
