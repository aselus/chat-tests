package main

import (
	"gitlab.com/aselus/chat-tests/experiment_chat/chatbin/chatbincmd"
)

func main() {
	chatbincmd.Execute()
}
