package gogo_drpc_codec

import (
	"encoding/json"
	"github.com/gogo/protobuf/proto"
)

func Marshal(msg interface{}) ([]byte, error) {
	return msg.(proto.Marshaler).Marshal()
}

func Unmarshal(buf []byte, msg interface{}) error {
	return msg.(proto.Unmarshaler).Unmarshal(buf)
}

func JSONMarshal(msg interface{}) ([]byte, error) {
	return msg.(json.Marshaler).MarshalJSON()
}

func JSONUnmarshal(buf []byte, msg interface{}) error {
	return msg.(json.Unmarshaler).UnmarshalJSON(buf)
}
