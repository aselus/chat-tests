// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package chat_api

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi(in *jlexer.Lexer, out *Empty) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi(out *jwriter.Writer, in Empty) {
	out.RawByte('{')
	first := true
	_ = first
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Empty) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Empty) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Empty) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Empty) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi(l, v)
}
func easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi1(in *jlexer.Lexer, out *ChatMessageRequest) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "lastSeenMsg":
			if in.IsNull() {
				in.Skip()
				out.LastSeenMsg = nil
			} else {
				out.LastSeenMsg = in.Bytes()
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi1(out *jwriter.Writer, in ChatMessageRequest) {
	out.RawByte('{')
	first := true
	_ = first
	if len(in.LastSeenMsg) != 0 {
		const prefix string = ",\"lastSeenMsg\":"
		first = false
		out.RawString(prefix[1:])
		out.Base64Bytes(in.LastSeenMsg)
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v ChatMessageRequest) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v ChatMessageRequest) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *ChatMessageRequest) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *ChatMessageRequest) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi1(l, v)
}
func easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi2(in *jlexer.Lexer, out *ChatMessage) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "message_id":
			if in.IsNull() {
				in.Skip()
				out.MessageId = nil
			} else {
				out.MessageId = in.Bytes()
			}
		case "body":
			out.Body = string(in.String())
		case "origin":
			if in.IsNull() {
				in.Skip()
				out.Origin = nil
			} else {
				out.Origin = in.Bytes()
			}
		case "grouping":
			if in.IsNull() {
				in.Skip()
				out.Grouping = nil
			} else {
				out.Grouping = in.Bytes()
			}
		case "received_time":
			out.ReceivedTime = int64(in.Int64())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi2(out *jwriter.Writer, in ChatMessage) {
	out.RawByte('{')
	first := true
	_ = first
	if len(in.MessageId) != 0 {
		const prefix string = ",\"message_id\":"
		first = false
		out.RawString(prefix[1:])
		out.Base64Bytes(in.MessageId)
	}
	if in.Body != "" {
		const prefix string = ",\"body\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.String(string(in.Body))
	}
	if len(in.Origin) != 0 {
		const prefix string = ",\"origin\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Base64Bytes(in.Origin)
	}
	if len(in.Grouping) != 0 {
		const prefix string = ",\"grouping\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Base64Bytes(in.Grouping)
	}
	if in.ReceivedTime != 0 {
		const prefix string = ",\"received_time\":"
		if first {
			first = false
			out.RawString(prefix[1:])
		} else {
			out.RawString(prefix)
		}
		out.Int64(int64(in.ReceivedTime))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v ChatMessage) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi2(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v ChatMessage) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson49961b36EncodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi2(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *ChatMessage) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi2(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *ChatMessage) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson49961b36DecodeGitlabComAselusChatTestsExperimentChatChatCommonChatApi2(l, v)
}
