package chat_client

import (
	"bytes"
	"container/list"
	"context"
	"encoding/base32"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/aselus/chat-tests/experiment_chat/chat_common/chat_api"
	"gitlab.com/aselus/chat-tests/experiment_chat/chat_common/security_api"
	"go.bryk.io/pkg/log"
	"go.bryk.io/pkg/net/drpc"
	"go.bryk.io/pkg/net/drpc/middleware/client"
	"strings"
)

type ChatApp struct {
	drpcClient     *drpc.Client
	chatClient     chat_api.DRPCChatClient
	securityClient security_api.DRPCSecurityClient

	conversationContents ConversationContents
	activeChatIndex      map[string]int

	// ui elements
	guiApp      *tview.Application
	chats       *tview.List
	chatInput   *tview.InputField
	chatTextBox *tview.TextView
}

type ConversationContents map[string]Grouping
type Grouping struct {
	*list.List
	Name string
}

func (chatApp *ChatApp) NewChat(key string, name string, lastMsg string) Grouping {
	chatApp.chats.AddItem(name, lastMsg, 0, nil)
	chatApp.activeChatIndex[key] = chatApp.chats.GetItemCount() - 1
	conversation := Grouping{
		List: list.New(),
		Name: name,
	}
	chatApp.conversationContents[key] = conversation
	return conversation
}

func Start() error {
	securityClient, err := drpc.NewClient("tcp", ":8995",
		drpc.WithProtocolHeader(),
		drpc.WithClientMiddleware(
			client.Logging(log.WithZero(log.ZeroOptions{true, "error", nil}).Sub(log.Fields{"component": "server"}), nil),
			client.PanicRecovery(),
		),
	)
	if err != nil {
		panic(err)
	}
	securityApi := security_api.NewDRPCSecurityClient(securityClient)
	clientId := base32Uuid(uuid.NewV4().Bytes())
	tokeResponse, err := securityApi.Authenticate(context.Background(), &security_api.AuthenticationRequest{
		UserId:       clientId,
		Verification: "",
	})
	if err != nil {
		panic(err)
	}
	securityClient.Close()

	drpcClient, err := drpc.NewClient("tcp", ":8998",
		drpc.WithProtocolHeader(),
		drpc.WithClientMiddleware(
			client.Metadata(map[string]string{
				"auth.token": tokeResponse.Token,
			}),
			client.Logging(log.WithZero(log.ZeroOptions{true, "error", nil}).Sub(log.Fields{"component": "server"}), nil),
			client.PanicRecovery(),
		),
	)
	if err != nil {
		panic(err)
	}
	defer drpcClient.Close()

	chatApp := ChatApp{
		conversationContents: make(ConversationContents),
		activeChatIndex:      make(map[string]int),
		chatClient:           chat_api.NewDRPCChatClient(drpcClient),
		drpcClient:           drpcClient,
	}

	chatApp.startUI()

	return nil
}

func base32Uuid(value []byte) string {
	id := uuid.Must(uuid.FromBytes(value))
	var buf bytes.Buffer
	encoder := base32.NewEncoder(base32.StdEncoding.WithPadding(base32.NoPadding), &buf)
	encoder.Write(id.Bytes())
	encoder.Close()
	return buf.String()
}

func (chatApp *ChatApp) StartServerInteraction() {
	// get the last "n" messages that one might have missed
	responses, err := chatApp.chatClient.GetMessages(context.Background(), &chat_api.ChatMessageRequest{LastSeenMsg: uuid.NewV4().Bytes()})
	if err != nil {
		return
	}

	// this is just to write the uuids in base32

	for response, err := responses.Recv(); err == nil; response, err = responses.Recv() {

		groupingId := base32Uuid(response.Grouping)
		var grouping Grouping
		if receivedGrouping, ok := chatApp.conversationContents[groupingId]; !ok {
			grouping = chatApp.NewChat(groupingId, groupingId, response.Body)
		} else {
			grouping = receivedGrouping
			chatApp.chats.SetItemText(chatApp.activeChatIndex[groupingId], groupingId, response.Body)
		}
		chatApp.guiApp.Draw()
		if grouping.Len() > 100 {
			grouping.Remove(grouping.Front())
		}
		grouping.PushBack(response)

		if main, _ := chatApp.chats.GetItemText(chatApp.chats.GetCurrentItem()); main == groupingId {
			chatApp.updateTextViewWith(grouping)
		}
	}
}

func (chatApp *ChatApp) updateTextViewWith(grouping Grouping) {
	builder := strings.Builder{}
	for element := grouping.Front(); element != nil; element = element.Next() {
		message := element.Value.(*chat_api.ChatMessage)
		builder.WriteString(base32Uuid(message.Origin))
		builder.WriteString("\n\t")
		builder.WriteString(message.Body)
		builder.WriteString("\n")
	}
	chatApp.chatTextBox.SetText(builder.String())
}

func (chatApp *ChatApp) selectedChat(index int, mainText string, _ string, _ rune) {
	if grouping, ok := chatApp.conversationContents[mainText]; ok {
		chatApp.updateTextViewWith(grouping)
		chatApp.guiApp.SetFocus(chatApp.chatInput)
	}
}

func (chatApp *ChatApp) chatEntryDone(key tcell.Key) {
	if key == tcell.KeyEnter {
		chatApp.chatInput.SetText("")
	}
}

func (chatApp *ChatApp) textBoxChanged() {
	chatApp.guiApp.Draw()
}

func (chatApp *ChatApp) startUI() {
	chatApp.guiApp = tview.NewApplication()

	chatApp.chats = tview.NewList()
	chatApp.chats.SetSelectedFunc(chatApp.selectedChat)
	chatsFrame := tview.NewFrame(chatApp.chats).SetBorders(1, 1, 0, 0, 1, 1)
	chatsFrame.SetBorder(true).SetTitle("Active Chats")

	// chat entry
	chatApp.chatInput = tview.NewInputField()
	chatApp.chatInput.SetDoneFunc(chatApp.chatEntryDone)
	chatApp.chatInput.SetPlaceholder("Enter your chat message here")
	inputsFrame := tview.NewFrame(chatApp.chatInput).SetBorders(1, 0, 0, 0, 3, 1)

	// text box
	chatApp.chatTextBox = tview.NewTextView()
	chatApp.chatTextBox.SetBorder(true)
	chatApp.chatTextBox.SetBackgroundColor(tcell.ColorDarkOliveGreen)
	chatApp.chatTextBox.SetChangedFunc(chatApp.textBoxChanged)

	flex := tview.NewFlex().AddItem(chatsFrame, 0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(chatApp.chatTextBox, 0, 3, false).
			AddItem(inputsFrame, 3, 1, true), 0, 4, false)
	chatApp.guiApp.EnableMouse(true)
	go chatApp.StartServerInteraction()
	if err := chatApp.guiApp.SetRoot(flex, true).SetFocus(chatApp.chatInput).Run(); err != nil {
		panic(err)
	}
}
